<?php require_once "./code.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: Activity</title>
</head>
<body>
    <div>
        <h1>Building</h1>
        <p>The name of the building <?php echo $building->getName();?>.</p>
        <p>The building has <?php echo $building->getFloors();?> floors.</p>
        <p>The <?php echo $building->getName()?> is located at <?php echo $building->getAddress();?> floors.</p>
        <?php echo $building->setName('Caswyn Complex')?>
        <p>The name of the building has been changed to <?php echo $building->getName();?> </p>

    </div>

    <div>
        <h1>Condominium</h1>
        <p>The name of the condominium <?php echo $condominium->getName();?>.</p>
        <p>The condominium has <?php echo $condominium->getFloors();?> floors.</p>
        <p>The <?php echo $condominium->getName()?> is located at <?php echo $condominium->getAddress();?> floors.</p>
        <?php echo $condominium->setName('Enzo Tower')?>
        <p>The name of the condominium has been changed to <?php echo $condominium->getName();?> </p>

    </div>
</body>
</html>
