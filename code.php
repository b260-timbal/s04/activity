<?php 

class Building {

    // Properties
    protected $name;
    protected $floors;
    protected $address;
 
    // Constructor
    public function __construct($name, $floors  ,$address ) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // getter and setter for name
    public function getName() {
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    // getter and setter for floors
    public function getFloors() {
        return $this->floors;
    }

    public function setFloors($floors){
        $this->floors = $floors;
    }

    // getter and setter for address
    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address){
        $this->address = $address;
    }
}

class Condominium extends Building {
    // getter and setter for name
    public function getName() {
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    // getter and setter for floors
    public function getFloors() {
        return $this->floors;
    }

    public function setFloors($floors){
        $this->floors = $floors;
    }

    // getter and setter for address
    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address){
        $this->address = $address;
    }
}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines'); 

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

?>